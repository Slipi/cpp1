#include <iostream>
#include <cmath>
class Vector {
public:
	Vector(float x, float y, float z) {
		this->x = x;
		this->y = y;
		this->z = z;
	}
	operator float() {
		return sqrt(x * x + y * y + z * z);
	}
	friend Vector operator+(const Vector& a, const Vector& b);
	friend Vector operator-(const Vector& a, const Vector& b);
	friend Vector operator*(const Vector& a, int num);
	friend std::ostream& operator<<(std::ostream& out, const Vector& a);
	friend std::istream& operator>>(std::istream& in, Vector& a);
	float operator[](int index) {
		switch (index){
		case 0:
			return x;
			break;
		case 1:
			return y;
			break;
		case 2:
			return z;
			break;
		default:
			std::cout << "index error";
			return 0;
			break;
		}
	}
	friend bool operator>(const Vector& a, const Vector& b);
private:
	float x, y, z;
};

Vector operator+(const Vector& a, const Vector& b){
	return Vector(a.x + b.x, a.y + b.y, a.z + b.z);
}
Vector operator-(const Vector& a, const Vector& b) {
	return Vector(a.x - b.x, a.y - b.y, a.z - b.z);
}
Vector operator*(const Vector& a, int num) {
	return Vector(a.x * num, a.y * num, a.z * num);
}
std::ostream& operator<<(std::ostream& out, const Vector& a) {
	out << a.x << ' ' << a.y << ' ' << a.z;
	return out;
}
std::istream& operator>>(std::istream& in, Vector& a) {
	in >> a.x >> a.y >> a.z;
	return in;
}
bool operator>(const Vector& a, const Vector& b) {
	return false;
}

int main() {
	Vector v1(0, 1, 2);
	Vector v2(3, 4, 5);
	std::cin >> v1;
	std::cout << v1 - v2;
	return 0;
}